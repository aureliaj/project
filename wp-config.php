<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Skrypt wp-config.php używa tego pliku podczas instalacji.
 * Nie musisz dokonywać konfiguracji przy pomocy przeglądarki internetowej,
 * możesz też skopiować ten plik, nazwać kopię "wp-config.php"
 * i wpisać wartości ręcznie.
 *
 * Ten plik zawiera konfigurację:
 *
 * * ustawień MySQL-a,
 * * tajnych kluczy,
 * * prefiksu nazw tabel w bazie danych,
 * * ABSPATH.
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'zadanie');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'zadanie');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', 'qwerty123');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'localhost');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8mb4');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Kq>P}5^(<-Q%p]MDYD!Ue@_=^{r^/LZ`FJ7VgOJxFdu&YNUaStGr^%?vm|M)ztS/');
define('SECURE_AUTH_KEY',  '(-RhoIip%8H}L9FB0zqXb13~;Vu) _FbWE`sL:RV^BJqa~877)0}#c@!EYC$lJ$s');
define('LOGGED_IN_KEY',    'f-}/_aGcbR<U9PWY,FBiNGVMI9*LK`s+wHv(tq?;QRxuh!ct* |2`$N@gKvc*^xU');
define('NONCE_KEY',        ')|uo+L+3F,RP25mJ&Q,zeX9D7i$,P.~]7)K_}R*|em}gmMyh#|1+xM(H{kMPIEr~');
define('AUTH_SALT',        '*ozYAR^N@F?DbTLmRX/K9g&NcXoHZ5p7xUoG6E|,I`aNpxM=`Rs pu>@J$-tO+KT');
define('SECURE_AUTH_SALT', ']Oe#Q>y^tLu?P[$vE</.E2JJ-u,sIr-b,bSZSUvmZ#U$Exj=ivEvuh,7##FvRM6d');
define('LOGGED_IN_SALT',   'A-)gs;.be:-2/&-GHAYx)W%sM&?JFUlw30pVG)(n=0hV+sJmC^iXK4w+Yd.SP{)q');
define('NONCE_SALT',       'X<H=n*PCtxq2#aw{2t9Na2qd%fI3F;dk#*gd-LIx-M|Hb|SY8Q!M)sX2_4#%`!Vc');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie
 * ostrzeżeń podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG podczas pracy nad nimi.
 *
 * Aby uzyskać informacje o innych stałych, które mogą zostać użyte
 * do debugowania, przejdź na stronę Kodeksu WordPressa.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
