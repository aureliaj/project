<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	
</head>

<body <?php body_class(); ?>>
<header class="js-nbar nbar container" id="navbar" role="banner">
<div class="site-branding header row">
				<div class="header-logo four columns">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt=""></a>
				</div>
				<button id="menu-toggle" class="menu-toggle"><?php _e( 'Menu', 'zadanie' ); ?></button>
				<nav id="primary-navigation" class="site-navigation primary-navigation six columns" role="navigation">
				
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'menu_id' => 'primary-menu' ) ); ?>
				</nav>
				<div class="two columns header-social">
				<ul>
				<li><a href="">DK</a></li>
				<li><a href="">PL</a></li>
				<li><a href="">EN</a></li>
				</ul>
				</div>
			</div>
</header>
<?php if ( is_home() || is_front_page()) { ?>
<div id="owl-demo" class="owl-carousel owl-theme">
 
  <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/img/glowna_02.jpg" alt="">
  <div class="owl-demo-text container">We are your Team<br>
  <a href="">SPRAWDŹ NASZE MOŻLIWOŚCI</a></div>
  <span class="scroll-to"><img src="<?php echo get_template_directory_uri(); ?>/img/down.png" alt="">Scroll down to explore</span>
  </div>
  <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/img/ban-2.jpg" alt="">
  <div class="owl-demo-text container">We are your Team<br>
  <a href="">SPRAWDŹ NASZE MOŻLIWOŚCI</a></div>
  <span class="scroll-to"><img src="<?php echo get_template_directory_uri(); ?>/img/down.png" alt="">Scroll down to explore</span>
  </div>
   
</div>
<?php } else { ?>
<div class="thumbnail-top">
	<?php
	
echo '</div>';
}
	?>