<?php

function zadanie_scripts() {
    wp_enqueue_style( 'style-name', get_stylesheet_uri() );
    wp_enqueue_style( 'normalize', get_template_directory_uri() . '/css/normalize.css' );
    wp_enqueue_style( 'skeleton', get_template_directory_uri() . '/css/skeleton.css' );
    wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css' );
    wp_enqueue_style( 'owlcarouselcss', get_template_directory_uri() . '/css/owl.carousel.css' );
    wp_enqueue_style( 'owltheme', get_template_directory_uri() . '/css/owl.theme.css' );
    wp_enqueue_style( 'layaut', get_template_directory_uri() . '/css/layaut.css' );
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-1.9.1.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'owlcarousel', get_template_directory_uri() . '/js/owl.carousel.js', array(), '1.0.0', true );
    wp_enqueue_script( 'wow-min', get_template_directory_uri() . '/js/wow.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'wow-init', get_template_directory_uri() . '/js/wow.init.js', array(), '1.0.0', true );
    wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'zadanie_scripts' );


add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'zadanie' ),
		'social'  => __( 'Social Links Menu', 'zadanie' ),
	) );

if ( ! function_exists( 'zadanie_the_custom_logo' ) ) :
function zadanie_the_custom_logo() {
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}
}
endif;

function zadanie_get_first_url() {
	$content = get_the_content();
	$has_url = function_exists( 'get_url_in_content' ) ? get_url_in_content( $content ) : false;

	if ( ! $has_url )
		$has_url = zadanie_url_grabber();

	/** This filter is documented in wp-includes/link-template.php */
	return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}	
function blogspace_url_grabber() {
	if ( ! preg_match( '/<a\s[^>]*?href=[\'"](.+?)[\'"]/is', get_the_content(), $matches ) )
		return false;

	return esc_url_raw( $matches[1] );
}	
?>