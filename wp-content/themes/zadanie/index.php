<?php get_header(); ?>
  
    <div class="top-section">
	
	
<div class="value-props row">	
<?php
 query_posts('cat=2');
 while (have_posts()) : the_post();
 echo "<div class='four columns top-section-blocks  wow bounceInUp'>";
 ?>
 <div class="top-section-blocks-text">
 <?php
echo "<h3>";
 the_title();
 echo "</h3>";
 ?>
 <div class="top-section-blocks-text--text">
 <?php
 the_content( '', TRUE );
  ?>
  </div>
<a href="<?php echo get_permalink() ?>"><span class="top-section-blocks-more"></span></a></div>
 <?php
 the_post_thumbnail();

 echo "</div>";
 endwhile;;
?>
</div>	

    </div>
  <div class="container">


    <!-- Why use Skeleton -->
    <div class="docs-section row" id="intro">
		
          <div class="one columns docs-section-box"></div>
          <div class="ten columns docs-section-box-center wow bounceInUp"><h6 class="docs-header">Sprawdzony partner w realizacji projektów offshorowych i specjalistycznych</h6>
      <p>
		Firma GS Seacon zajmuje się tworzeniem zaawansowanych konstrukcji offshorowych i usługami wykonawczymi przy inwestycjach 
		budownictwa kubaturowego. Na przestrzeni lat pomogliśmy zrealizować wiele projektów we współpracy z międzynarodowymi 
		firmami. Jesteśmy pewnym partnerem, który podejmuje wyzwania i realizuje je na najwyższym poziomie.</p></div>
		  <div class="one columns docs-section-box"></div>
        
	
      
      
 
    </div>
    </div>
<div class="full-width">	
<div class="full-width-bg"></div>	
	<div class="container">

<?php $recent = new WP_Query("page_id=38"); 
      while($recent->have_posts()) : $recent->the_post();?>	
<div class="offer-section row" id="offer">
		<div class="one columns offer-section-box offer-section-box-one"></div>
          <div class="five columns offer-section-box wow flipInX">
		  



    <h3><?php echo get_field( "drugi_tytuł" ); ?> <br> <?php echo get_field( "drugi_tytuł_cz._2" ); ?> </h3>
      <?php the_content( '', TRUE ); ?>
        
        <a class="offer-section-box-anhor" href="<?php echo get_page_link($item->ID) ?>">Phasellus nisi metus</a>
      
   
		  </div>
          <div class="six columns offer-section-box wow bounceInUp"><?php echo get_the_post_thumbnail(); ?></div>
		  
        
	
      
      
 
    </div>
	<?php endwhile; ?>
    </div>
    </div>
	
		<div class="container">
	
<div class="product-section row wow bounceInUp" id="product">
<?php $recent = new WP_Query("page_id=43"); 
      while($recent->have_posts()) : $recent->the_post();?>	
<div class="five columns product-section-box product-section-box-five"><?php echo get_the_post_thumbnail(); ?></div>
		
          <div class="four columns product-section-box product-section-box-four">
		  <h3><?php echo get_field( "drugi_tytuł" ); ?> <br> <?php echo get_field( "drugi_tytuł_cz._2" ); ?></h3>
<?php the_content( '', TRUE ); ?>
<a class="product-section-box-anhor" href="<?php echo get_page_link($item->ID) ?>">Phasellus nisi metus</a>
		  
		  </div>
		  <div class="three columns product-section-box product-section-box-three"></div>

 <?php endwhile; ?>
    </div>
	
	<div class="sukces-section row ">
	<div class="sukces-section-box wow bounceInUp"><span>30</span>
zrealizowanych <br>
inwestycji</div>
<div class="sukces-section-box wow bounceInUp"><span>15</span>

LAT DOŚWIADCZENIA<br></div>
<div class="sukces-section-box wow bounceInUp"><span>320</span>

wykwalifikowanych<br> 
pracowników </div>
<div class="sukces-section-box wow bounceInUp"><span>300</span>

PRZERÓB TON STALIMIESIĘCZNIE</div>
<div class="sukces-section-box wow bounceInUp"><span>911</span>

Aliquam erat volutpat. <br>
Phasellus </div>
	
	</div>
	
  


    <!-- Why use Skeleton -->
    <div class="finibus-section row" id="intro">
		
          <div class="one columns finibus-section-box"></div>
          <div class="ten columns finibus-section-box-center wow flipInX">
		  <?php $recent = new WP_Query("page_id=47"); 
      while($recent->have_posts()) : $recent->the_post();?>	
		  
		  <h3><?php echo get_field( "drugi_tytuł" ); ?> <br> <?php echo get_field( "drugi_tytuł_cz._2" ); ?></h3>
      <a href="<?php echo get_page_link($item->ID) ?>">Aliquam erat</a>
	  
	  <?php endwhile; ?>
	  </div>
		  <div class="one columns finibus-section-box"></div>
        
	
    </div>
	
	 <div class="around-section row wow bounceInRight" id="intro">
		
          
          <div class="ten columns around-section-box-center wow bounceInUp"><img class="value-img" src="http://localhost/project/wp-content/uploads/2016/11/glowna_26.jpg"></div>
		  <div class="two columns around-section-box"></div>
       
          <div class="four columns around-section-box--top">
	<h4>Around 90 percent of world
trade is carried by sea</h4>

<p>Pellentesque vitae elit lectus. Aliquam varius
congue nisi et consequat. Suspendisse rhoncus
dictum. Integer quis mi quis risus convallis
congue viverra nec sapien. Quisque gravida
urna nibh, ac malesuada ante dapibus suscipit.</p>

	</div> 
 
    </div>
	
	<div class="product-section row wow bounceInRight" id="workers">
	
	<?php $recent = new WP_Query("page_id=75"); 
      while($recent->have_posts()) : $recent->the_post();?>	
<div class="five columns product-section-box product-section-box-five"><?php echo get_the_post_thumbnail(); ?></div>
		
          <div class="four columns product-section-box product-section-box-four">
		  <img src="http://localhost/project/wp-content/uploads/2016/11/glowna_33.png" width="71" height="70" alt="">
		  <h3><?php echo get_field( "drugi_tytuł" ); ?> <br> <?php echo get_field( "drugi_tytuł_cz._2" ); ?></h3>
<?php the_content( '', TRUE ); ?>
<a class="product-section-box-anhor" href="<?php echo get_page_link($item->ID) ?>">Phasellus nisi metus</a>
		  
		  </div>
		  <div class="three columns docs-section-box docs-section-box-three"></div>

 <?php endwhile; ?>
	
	
		

 
    </div>
	
	
	<div class="numbers-section row wow bounceInUp" id="workers">
	<div class="two columns numbers-section-box numbers-section-two"></div>
		<div class="four columns numbers-section-box numbers-section-four"><h4>Fusce suscipit porta turpis, </h4><h4>
non vehicula elit. </h4></div>
		
          <div class="four columns numbers-section-box numbers-section-four">
		  <div class="four columns numbers-section-box">
		  Aliquam erat
<span>729</span>
		  
		  </div><div class="four columns numbers-section-box">
		  
		  Aliquam erat
<span>356</span>
		  </div><div class="four columns numbers-section-box">
		  Aliquam erat
<span>158</span>
		  
		  </div>
		  
		  </div>
		  <div class="one columns numbers-section-box numbers-section-one"></div>

 
    </div>
	
	
	<div class="slider-section row wow bounceInUp" id="workers">
	<div id="owl-slider">
          
		<div class="item"><img src="http://localhost/project/wp-content/uploads/2016/11/glowna_37.png" alt=""> <h5>Road tunnel,<br>
Køge North</h5>

<div class="owl-slider-image-text">Total length: 400m<br>
Tunnel length: 150m</div>
</div>
		<div class="item"><img src="http://localhost/project/wp-content/uploads/2016/11/glowna_39.png" alt="">
		<h5>Bicycle and<br>
pedestrians bridge,
Copenhagen</h5>

<div class="owl-slider-image-text">Bridge length: 140m<br>
Bridge width: 7m</div>
		
		</div>
		<div class="item"><img src="http://localhost/project/wp-content/uploads/2016/11/glowna_42.png" alt="">
		<h5>Helsingør-<br>
motorvejen</h5>

<div class="owl-slider-image-text">3 road bridges over <br>
the highway</div>
		
		</div>
		<div class="item"><img src="http://localhost/project/wp-content/uploads/2016/11/glowna_44.png" alt="">
		<h5>Promenade in<br>
Marmormolen 1st<br>
stage, Copenhagen</h5>

<div class="owl-slider-image-text">Promenade in Marmormol<br>
en 1st stage, Copenhagen</div>
		
		</div>
		<div class="item"><img src="http://localhost/project/wp-content/uploads/2016/11/2.jpg" alt="">
		<h5>New bridge Terslev<br>
Skolevej, Haslev
</h5>

		
		</div>
		
 
	</div>

 
    </div>
	
	<div class="people-section row" id="people">
	<div id="owl-people">
	<?php
 query_posts('cat=3');
 while (have_posts()) : the_post();
 echo "<div class='item wow bounceInUp'>";
 ?>
 <h6><?php echo get_cat_name(3); ?></h6>
 <p><?php echo get_field( "krotki_opis" ); ?></p>
 <div class="six columns owl-people-text">
  <?php
 the_post_thumbnail();

?>
</div><div class="five columns owl-people-text">
		<p><?php echo get_field( "imie" ); ?><br><span><?php echo get_field( "firma" ); ?></span></p>
		</div>

</div>
<?php
 endwhile;;
?>
          			
 
	</div>

 
    </div>	
	<div class="logos-section row" id="logos">
	<img src="<?php echo get_template_directory_uri(); ?>/img/logo-1.jpg" alt="" class="img wow flipInX">
	<img src="<?php echo get_template_directory_uri(); ?>/img/logo-2.jpg" alt="" class="img wow flipInX">
	<img src="<?php echo get_template_directory_uri(); ?>/img/logo-3.jpg" alt="" class="img wow flipInX">
	<img src="<?php echo get_template_directory_uri(); ?>/img/logo-4.jpg" alt="" class="img wow flipInX">
	<img src="<?php echo get_template_directory_uri(); ?>/img/logo-5.jpg" alt="" class="img wow flipInX">
	<img src="<?php echo get_template_directory_uri(); ?>/img/logo-6.jpg" alt="" class="img wow flipInX">
	
    </div>	
    </div>	
	


<?php get_footer(); ?>
