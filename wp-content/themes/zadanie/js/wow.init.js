new WOW().init();

wow = new WOW(
    {
        boxClass:     'wow2',      // default
        animateClass: 'animated', // default
        offset:       10,          // default
        mobile:       true,       // default
        live:         true        // default
    }
);
wow.init();
