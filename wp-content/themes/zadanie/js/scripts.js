jQuery(document).ready(function( $ ) {
 
  $("#owl-demo").owlCarousel({
 
      autoPlay : 4000,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
  });
  
  $("#owl-slider").owlCarousel({
 
     
		navigation : true,
      items : 4,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]
 
  });
  
  
  $("#owl-people").owlCarousel({
 
      autoPlay : 5000,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true
 
  });
  
  
  $(".menu-toggle").click(function(){
        $("nav").toggle();

    });
	
	
	var nav = $('.site-branding');
	
	$(window).scroll(function () {
		if ($(this).scrollTop() > 536) {
			nav.addClass("f-nav");
		} else {
			nav.removeClass("f-nav");
		}
	});
	
	$(".scroll-to").click(function() {
    $('html, body').animate({
        scrollTop: $(".around-section-box--top").offset().top
    }, 2000);
});
 
});